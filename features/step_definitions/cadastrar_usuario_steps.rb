Dado("que eu tenha acesso no site tradersclub") do
  	@cadastrar = Cadastrar.new
    @cadastrar.load
end

Quando("realizar um cadastro de novo usuario") do
  	@cadastrar.cadastrar_novo 'teste3@teste.com.br', 'teste3', 'Fe363620', 'Fe363620', 'Tester', 'Testador', '00000000013', '11999999999', '01011918', 'Básica', 'Masculino'
 end

Entao("valido que o sistema cadastrara um novo usuario") do
	@logar = Logar.new
    @validar = Validar.new
	@logar.load
	@logar.logar 'teste2','Fe363620'
end