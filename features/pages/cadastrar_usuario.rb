class Cadastrar<SitePrism::Page

	set_url 'https://tc.tradersclub.com.br/login'
	
	#step1
	element :cadastro_link, :xpath, '//*[@id="root"]/div/div[1]/div/div/div/div/div/a'
	element :email_field, :xpath, '//*[@id="root"]/div/div[1]/div/div[2]/div/div/div/form/div[2]/div[1]/div[1]/div/input'
	element :user_field, '#login-user'
	element :senha_field, :css, '#root > div > div.row.content > div > div.login-page > div > div > div > form > div.step-1 > div:nth-child(2) > div:nth-child(1) > div > input'
	element :confirmsenha_field, :xpath, '//*[@id="root"]/div/div[1]/div/div[2]/div/div/div/form/div[2]/div[2]/div[2]/div/input'
	element :termo_checkbox, :xpath, '//*[@id="termos"]'
	element :proximo_button, :xpath, '//*[@id="root"]/div/div[1]/div/div[2]/div/div/div/form/div[2]/div[4]/div/button'
	


	#step2
	element :nome_field, '#login-name'
	element :sobrenome_field, '#login-lastname'
	element :cpf_field, :xpath,'//*[@id="root"]/div/div[1]/div/div[2]/div/div/div/form/div[2]/div[2]/div[3]/div/input'
	element :celular_field, :xpath, '//*[@id="root"]/div/div[1]/div/div[2]/div/div/div/form/div[2]/div[2]/div[4]/div/input'
	element :data_nasc_field, '#login-birthdate'
	element :genero_list, :xpath, '//*[@id="root"]/div/div[1]/div/div[2]/div/div/div/form/div[2]/div[2]/div[6]/div/div/div/select'
	element :proximo_button1, :xpath, '//*[@id="root"]/div/div[1]/div/div[2]/div/div/div/form/div[2]/div[3]/div/button[2]'
	#step3
	element :experiencia_list, :xpath, '//*[@id="root"]/div/div[1]/div/div[2]/div/div/div/form/div[2]/div[1]/div/div/div/div/select'
	element :ativos_checkbox, '#Ações'
	element :metodologia_checkbox, :xpath, '//*[@id="root"]/div/div[1]/div/div[2]/div/div/div/form/div[2]/div[2]/div[2]/div/div/div/div/label[1]'
	element :enviar_button, :xpath, '//*[@id="root"]/div/div[1]/div/div[2]/div/div/div/form/div[2]/div[3]/div/button[2]'


	def cadastrar_novo(email, user, senha, confirmsenha, nome, sobrenome, cpf, celular, datanasc, experiencia, genero)
		#step1
		cadastro_link.click
		email_field.set(email)
		user_field.set(user)
		senha_field.set(senha)
		confirmsenha_field.set(confirmsenha)
		termo_checkbox.click
		proximo_button.click
		#step2
		nome_field.set(nome)
		sobrenome_field.set(sobrenome)
		cpf_field.set(cpf)
		celular_field.set(celular)
		data_nasc_field.set(datanasc)
		genero_list.select(genero).click
		proximo_button1.click
		#step3
		experiencia_list.select(experiencia)
		ativos_checkbox.click
		metodologia_checkbox.click
		enviar_button.click
	end
 
 # def teste
 # 	if wait_until_proximo_button_visible(10)
 # 	puts "OK"
 # 	else
 # 	puts "NOK"
 # end
# end

end
