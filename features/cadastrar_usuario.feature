#language: pt
#utf-8

@cadastrar
Funcionalidade: Cadastrar usuario
	Eu como usuario do site tradersclub
	Quero realizar um cadastro de novo usuario
	Para validar o sistema de cadastro de usuario

	
	Cenario: 
		Dado que eu tenha acesso no site tradersclub
		Quando realizar um cadastro de novo usuario
		Entao valido que o sistema cadastrara um novo usuario